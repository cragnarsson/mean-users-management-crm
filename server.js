// BASE SETUP
// ======================================

// CALL THE PACKAGES --------------------
var express 	= require('express'), 		// Load Express
	app 		= express(), 				// Define our app using express
	bodyParser  = require('body-parser'), 
	morgan 		= require('morgan'),		// Used to see requests
	mongoose 	= require('mongoose'), 		// For working w/ database
	path 		= require('path'),
	config 		= require('./config');


// APP CONFIGURATION ==================
// ====================================

// Connect to our database(local)
mongoose.connect(config.database);

// Log all requets to the console
// ======================================
app.use(morgan('dev'));

// Set static file location
// Used for requests that our frontend will make
app.use(express.static(__dirname + '/public'));

// Use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configure app to handle CORS requets
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \
22 Authorization');
	next();
});

// ROUTES =================
// ====================================

// API ROUTES
var apiRoutes = require('./app/routes/api')(app, express);
app.use('/api', apiRoutes);

// MAIN CATCHALL ROTUE -------------------
// SEND USERS TO FRONTEND
app.get('*', function(req, res) {
	res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

// START THE SERVER  =================
// ======================================
app.listen(config.port);
console.log('Magic @ ' + config.port);