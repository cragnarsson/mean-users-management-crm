// Define configuration here

module.exports = {
	'port': process.env.PORT || 8888, // Set the port for our app
	'database': 'localhost:27017/API-test',
	'secret': 'thisisthefirstnodejsapiihavemade'
};