var User		= require('../models/user');
var jwt			= require('jsonwebtoken');
var config 		= require('../../config');

// Seccret for creating tokens
var jwtSecret = config.secret;

module.exports = function(app, express) {
	
	// ROUTES
	// ======================================

	// Get an instance of the express router
	var apiRouter = express.Router();

	// Route for authenticating users
	apiRouter.post('/authenticate', function(req, res) {
		// Find the user
		User.findOne({
			username: req.body.username
		}).select('name username password').exec(function(err, user) {
			if (err) throw err;

			// If the user was not found
			if (!user) {
				res.json({ success: false, message: 'Authentication failed. User not found' });
			} else if (user) {
				// If the user was found
				var validPassword = user.comparePassword(req.body.password);
				if (!validPassword) {
					res.json({ success: false, message: 'Authentication failed. Wrong password' });
				} else {
					// Use is found and password is right
					// Create the token :) 
		var token = jwt.sign({
			name: user.name,
			username: user.username
		}, jwtSecret, {
						expiresInMinutes: 1440 // Expires in 24 hours
					});

					// Return the info, including the token as JSON
					res.json({
						success: true,
						message: 'Token sent.',
						token: token
					});
				}
			}
		});
	});

	// Middleware to use for all requests
	apiRouter.use(function(req, res, next) {
		
		// Check POST, URL parameters or HEADER  for token
		var token = req.body.token || req.param('token') || req.headers['x-access-token'];

		if (token) {
			// Verifies secret and checks exp
			jwt.verify(token, jwtSecret, function(err, decoded) {
				if (err) {
					return res.status(403).send({ succcess: false, message: 'Failed to authenticate token. Please request a new and try again.' });
				} else {
					// No error, everything is good.
					// Save to request for use in other routes
					req.decoded = decoded;
					next();
				}
			});
		} else {
			// There is no token
			// Return a HTTP response of 403 and a error message
			return res.status(403).send({ success: false, message: ' No token provided.' });
		}
	});

	//Index route
	apiRouter.get('/', function(req, res) {
		res.json({ message: 'Horray! Welcome to our API! :)' });
	});

	// On routes that end in /users
	// ======================================
	apiRouter.route('/users')
		// Create a user (Accessed at POST /api/users)
		.post(function(req, res) {
			// Create a new instance of the User model
			var user = new User();

			// Set the users info (from the request)
			user.name = req.body.name;
			user.username = req.body.username;
			user.password = req.body.password;

			// Save the user and check for errors
			user.save(function(err) {
				if (err) {
					// User already exist
					if (err.code == 11000)
						return res.json({ success: false, message: 'A user with that username already exists.' });
					else
						return res.send(err);
				}
				res.json({ message: 'User created!' });
			});
		})
		// Get all the users (Accessed at GET /api/users)
		.get(function(req, res) {
			User.find(function(err, users) {
				// If error, return the error
				if (err) res.send(err); 

				// Return the users
				res.json(users);
			});
	});
	// On routes that end in /users/:user_id
	// ======================================
	apiRouter.route('/users/:user_id')

		// Get user with the input id (Accessed at GET /api/users/:user_id)
		.get(function(req, res) {
			User.findById(req.params.user_id, function(err, user) {
				// If error, return the error
				if (err) res.send(err); 

				// Return that user
				res.json(user);
			});
		})
		// Update the user with the input id (Accessed at PUT /api/users/:user_id)
		.put(function(req, res) {
			// Use our user model to find the user we want
			User.findById(req.params.user_id, function(err, user) {
				// If error, return the error
				if (err) res.send(err);

				// Update the users info only if its new
				if (req.body.name) user.name = req.body.name;
				if (req.body.username) user.username = req.body.username;
				if (req.body.password) user.password = req.body.password;

				// Save the user
				user.save(function(err) {
					if (err) res.send(err);

					// Return a message
					res.json({ message: 'User updated! :) '});
				}); 
			});
		})
		// Delete the user with the input id (Accessed at DELETE /api/users/:user_id)
		.delete(function(req, res) {
			User.remove({_id: req.params.user_id}, function(err, user) {
				if (err) res.send(err);

				// Return a message
				res.json({ message: 'Successfully deleted' });
			});
		});
	return apiRouter;
};