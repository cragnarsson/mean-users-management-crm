angular.module('app.routes', ['ngRoute'])

.config(function($routeProvider, $locationProvider) {

	$routeProvider
		// Home page route
		.when('/', {
			templateUrl: 'app/views/pages/home.html'
		})
		// Login page
		.when('/login', {
			templateUrl: 'app/views/pages/login.html',
			controller: 'mainController',
			controllerAs: 'login'
		});

		// Create nice urls
		$locationProvider.html5Mode(true);
});